import React from 'react';
import './Pomodoro.scss';
import Clock from './components/clock/Clock';
import Navigation from './components/navigation/Navigation';
import { ClockListItem } from '../../shared/models/IClockList';
import { ClockServices } from './components/clock/services/ClockServices';

class Pomodoro extends React.Component<any, any> {

  constructor(props: Readonly<any>,
    private clockService: ClockServices) {
    super(props)
    this.clockService = new ClockServices();
    this.setInitialState();
  }

  private setInitialState() {
    const clockItem = this.clockService.getItemList(ClockListItem.POMODORO);
    this.state = {
      pickedTime: clockItem?.value,
      clockItem: clockItem
    }
  }

  public onListMenuClick = (itemClicked: ClockListItem) => {
    const clockItem = this.clockService.getItemList(itemClicked);
    console.log({pickedTime: clockItem?.value, clockItem: clockItem })
    this.setState({pickedTime: clockItem?.value, clockItem: clockItem });
  }

  render() {
    const { pickedTime, clockItem } = this.state;
    return (
      <div className={clockItem.class}>
        <div className="box">
          <Navigation />
          <Clock onListMenuClick={this.onListMenuClick} clockItem={clockItem} pickedTime={pickedTime}/>
        </div>
      </div>
    )
  }
}

export default Pomodoro;
