import React from "react";
import { LinearProgress } from '@material-ui/core';
import './ProgressBar.scss';
import { DateServices } from "../../../../shared/services/DateServices";
import { createMuiTheme, MuiThemeProvider, Theme } from '@material-ui/core/styles';

class ProgressBar extends React.Component<any, any> {

    private theme: Theme;

    constructor(props: Readonly<{}>,
        private dateService: DateServices) {
        super(props);
        this.dateService = new DateServices();
        this.theme = createMuiTheme();
        this.setProgressBarTheme();
    }

    private setProgressBarTheme() {
        this.theme = createMuiTheme({
            palette: {
                secondary: {
                    main: '#BDBDBD'
                }
              }
        });
    }

    private getValue(): number {
        const { actuallyTime, pickedTime } = this.props;
        const actuallytimeSeconds = this.dateService.getSecondsFromDate(actuallyTime);
        const pickedTimeSeconds = this.dateService.getSecondsFromDate(pickedTime);
        return 100 - Math.floor((actuallytimeSeconds * 100) / pickedTimeSeconds);
    }

    render() {
        return (
            <div className="progress">
                <MuiThemeProvider theme={this.theme}>
                    <LinearProgress color="secondary" variant="determinate" value={this.getValue()} />
                </MuiThemeProvider>
            </div>
        )
    }
}

export default ProgressBar;