
import React from 'react';
import './Navigation.scss';


const Navigation = () => {
    return (
        <nav className="navigation">
            <h1>Pomodoro Focus</h1>
        </nav>
    )
}

export default Navigation;