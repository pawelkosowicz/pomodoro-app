import React from 'react';
import './List.scss';
import { ClockListItem } from '../../../../../../shared/models/IClockList';

class List extends React.Component<any,any> {

    constructor(props: Readonly<{}>) {
        super(props)
    }

    public onItemClick = (option: ClockListItem,e?: any) => {   
        this.props.onItemClick(option);     
        this.setActiveItemClass(e);        
    }

    private setActiveItemClass = (e: MouseEvent) => {
        const target = e.target as HTMLElement;
        const children = target.parentElement?.children as HTMLCollection;
        Array.from(children).forEach((v)=> v.className = 'clock-list__item');
        target.classList.add("clock-list__item--active");
    }

    render() {
        return (
            <ul className="clock-list">
                <li className="clock-list__item clock-list__item--active" onClick={(e) => this.onItemClick(ClockListItem.POMODORO,e)}>{ClockListItem.POMODORO}</li>
                <li className="clock-list__item" onClick={(e) => this.onItemClick(ClockListItem.SHORT,e)}>{ClockListItem.SHORT}</li>
                <li className="clock-list__item" onClick={(e) => this.onItemClick(ClockListItem.LONG,e)}>{ClockListItem.LONG}</li>
            </ul>
        )
    }
}

export default List;