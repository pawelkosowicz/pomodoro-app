import React from 'react';

import './Timer.scss';

const Timer = ({time}: {time: string}) => {
    return (
        <div className="timer">
            {time}
        </div>
    )
}

export default Timer;