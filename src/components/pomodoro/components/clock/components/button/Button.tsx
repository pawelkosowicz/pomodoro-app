import React from 'react';
import './Button.scss';
import { IButtonClock, clockButton, ButtonClockClass, ButtonClockText } from '../../../../../../shared/models/IButton';

//props, state
class Button extends React.Component<any, IButtonClock> {
    constructor(props: Readonly<{}>) {
        super(props)
        this.state = {
            ...clockButton
        }        
    }

    public onButtonClick = (event: React.MouseEvent) => {
        const className = event.currentTarget.className as ButtonClockClass;
        this.setButtonClass(className);             
    }
    

    componentDidUpdate(previousProps: any, previousState: any){
        console.log(this.props.resetClock);
        if(this.props.resetClock === true && this.props.resetClock !== previousProps.resetClock) {
            this.setButtonClass(ButtonClockClass.ACTIVE)
        }
    }

    private setButtonClass = (className: ButtonClockClass) => {
        switch(className) {
            case ButtonClockClass.ACTIVE:                
                this.props.onButtonClick(ButtonClockText.DEFAULT);
                this.setState({className: ButtonClockClass.DEFAULT, text: ButtonClockText.DEFAULT})
            break;
            case ButtonClockClass.DEFAULT:
                this.props.onButtonClick(ButtonClockText.ACTIVE);
                this.setState({className: ButtonClockClass.ACTIVE, text: ButtonClockText.ACTIVE})
            break;    
        }       
    }

    render() {
        const {className, text} = this.state;
        return (
            <button className={className} onClick={this.onButtonClick}>{text}</button>
        )
    }
}

export default Button;