import React from 'react';
import Timer from './components/timer/Timer';
import List from './components/list/List';
import Tile from '../../../../shared/utils/components/tile/Tile';
import Button from './components/button/Button';
import { ClockListItem } from '../../../../shared/models/IClockList';
import './Clock.scss';
import { ButtonClockText } from '../../../../shared/models/IButton';
import { ClockServices } from './services/ClockServices';
import { DateServices } from '../../../../shared/services/DateServices';
import ProgressBar from '../progress-bar/ProgressBar';

//TODO add type for props and state
class Clock extends React.Component<any, any> {

    private tileClass: any = {
        padding: '20px',
        color: 'white',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }

    private resetClock: boolean;

    constructor(props: Readonly<{}>,
        private clockService: ClockServices,
        private dateService: DateServices) {
        super(props)
        this.clockService = new ClockServices();
        this.dateService = new DateServices();
        this.resetClock = false;
        this.state = {
            pickedTime: this.props.pickedTime,
            clockItem: this.props.clockItem
        }
    }

    public onListMenuClick = (itemClicked: ClockListItem) => {
        this.props.onListMenuClick(itemClicked);
        this.onButtonClick(ButtonClockText.DEFAULT);
        this.resetClock = true;   
    }

    public componentDidUpdate(previousProps: any, previousState: any) {
        if (previousProps.clockItem.value !== this.props.clockItem.value) {
            this.setState({pickedTime: this.props.pickedTime, clockItem: this.props.clockItem });
            this.clockService.clockStop();
            this.resetClock = false;
        }
    }

    private setClockState = () => {
        if(this.dateService.getSecondsFromDate(this.state.clockItem.value) === 0) { 
            this.clockService.clockStop(); 
            this.resetClock = true;
        } else {
            this.setState((prev: any) => {
                const clockItem =  prev.clockItem; 
                const value = this.dateService.substrSecondsFromMinutes(prev.clockItem.value, 1);
                return { clockItem: {...clockItem,value} }
            })
        }      
    }

    public onButtonClick = (buttonType: ButtonClockText) => {
        switch (buttonType) {
            case ButtonClockText.ACTIVE:
                this.clockService.clockStart(this.setClockState);               
                break;
            case ButtonClockText.DEFAULT:
                this.clockService.clockStop();
                break;
        }
    }

    render() {
        const { pickedTime, clockItem } = this.state;
        return (
            <React.StrictMode>
                <ProgressBar actuallyTime={clockItem.value} pickedTime={pickedTime}/>
                <div className="clock">
                    <Tile styles={this.tileClass}>
                        <List onItemClick={this.onListMenuClick} />
                        <Timer time={clockItem.value} />
                        <Button resetClock={this.resetClock} onButtonClick={this.onButtonClick} />
                    </Tile>
                </div>
            </React.StrictMode>
        )
    }
}

export default Clock;