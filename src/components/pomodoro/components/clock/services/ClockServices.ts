import {
  ClockListItem,
  clockListItem,
  ICLockListItem,
} from "../../../../../shared/models/IClockList";

export class ClockServices {

  private intervalID: any;

  constructor() { }
  
  public getItemListValue(text: ClockListItem): string | undefined {
    return clockListItem.find((v: ICLockListItem) => v.text === text)?.value;
  }

  public getItemList(text: ClockListItem): ICLockListItem | undefined {
    return clockListItem.find((v: ICLockListItem) => v.text === text);
  }

  public clockStart(setState: Function) {
    if(!this.intervalID) 
      this.intervalID = setInterval(() => setState() ,1000)    
  }

  public clockStop() {
    clearInterval(this.intervalID);
    this.intervalID = null;
  }

}
