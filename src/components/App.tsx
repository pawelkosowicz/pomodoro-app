import React from 'react';
import './App.scss';
import Pomodoro from './pomodoro/Pomodoro';

class App extends React.Component {

  constructor(props: Readonly<any>) {
    super(props)
  }

  render() {
    return (
      <main className="main">
        <Pomodoro />
      </main>
    )
  }
}

export default App;
