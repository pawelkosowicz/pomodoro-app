import React from 'react';
import './Tile.scss';
import { ITile } from '../../../models/ITile';

const Tile = ( {children, styles}: ITile ) => {
    
    return (
        <div className="tile" style={styles}>
            {children}
        </div>
    )
}

export default Tile;