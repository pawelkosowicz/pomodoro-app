import { IProps } from "./IProps";

export interface ITile extends IProps{
    styles: Object
}