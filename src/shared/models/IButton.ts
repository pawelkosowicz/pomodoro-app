export interface IButton<T, K> {    
    text: K;
    className: T;
}

export enum ButtonClockClass {
    DEFAULT = 'button',
    ACTIVE = 'button button--active'
}

export enum ButtonClockText {
    DEFAULT = 'START',
    ACTIVE = 'STOP'
}

export interface IButtonClock extends IButton<ButtonClockClass,ButtonClockText> {
}

export const clockButton: IButtonClock = {
    className: ButtonClockClass.DEFAULT,
    text: ButtonClockText.DEFAULT
}