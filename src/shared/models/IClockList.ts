export enum ClockListItem {
    POMODORO = 'Pomodoro',
    SHORT = 'Short Break',
    LONG = 'Long Break'
}

export interface ICLockListItem {
    text: ClockListItem,
    value: string,
    class: string
}

export const clockListItem: ICLockListItem[] = [
    {text: ClockListItem.POMODORO, value: '25:00', class: 'pomodoro'},
    {text: ClockListItem.SHORT, value: '00:05', class: 'pomodoro pomodoro--short'},
    {text: ClockListItem.LONG, value: '15:00', class: 'pomodoro pomodoro--long'}
]