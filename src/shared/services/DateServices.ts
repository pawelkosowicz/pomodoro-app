import moment from 'moment';

export class DateServices {

    private readonly FORMAT_MINUTES = 'mm:ss';

    public getFormatMinutes(minutes: number): string {
        return moment(minutes, 'minutes').format(this.FORMAT_MINUTES);
    }

    public substrSecondsFromMinutes(date: string, minusSeconds: number): string {
        let minutes = parseInt(date.substr(0,date.indexOf(':')),10);
        let seconds = parseInt(date.substr(date.indexOf(':') +1,date.length - 1),10);

        if(seconds>0) seconds -= minusSeconds;
         else if(minutes>0){
            seconds = 59;
            minutes -= minusSeconds;
        } else if(minutes === 0) {
            seconds = 0;
            minutes = 0;
        }
        return `${minutes<10?'0'+minutes: minutes}:${seconds<10?'0'+seconds: seconds}`;
    }

    public getSecondsFromDate(date: string): number {
        const dateElement = date.split(':');
        const minutes = Number.parseInt(dateElement[0],10);
        const seconds = Number.parseInt(dateElement[1],10);

        return (minutes * 60) + seconds;
    }
}